const ColorStack = require('./color');
const tempFetch = require('./tempFetch');
const logiled = require('logiled');
const key = require('./key');

logiled.init();

const app = require('express')();
app.get('/switch/cpu', (req, res) => {
  console.log("Switch CPU");
  tempFetch.mode = 'cpu';
  res.sendStatus(200);
});
app.get('/switch/gpu', (req, res) => {
  console.log("Switch GPU");
  tempFetch.mode = 'gpu';
  res.sendStatus(200);
});
app.listen(55556);

const colorStack = new ColorStack([50, 0, 0, 1]);
setInterval(() => key.blit(colorStack, true), 100);

// Number keys
colorStack.addColorCallback((x, y) => {
  if (y == 1 && x > 0 && x < 11) {
    if (tempFetch.temp >= 110)
      return [255, 0, 0, 1];
    if (x == Math.floor(tempFetch.temp/10))
      return [255, 0, 0, 1];
    return [255, 0, 0, 0.2];
  }
});

const ranges = [
  { start:  1, step: 2, color: [  0,   0, 255] },
  { start: 27, step: 1, color: [  0, 125, 255], min: 27 },
  { start: 40, step: 4, color: [  0, 255,   0], min: 40 },
  { start: 90, step: 1, color: [255,   0,   0], blink: true, min: 90 },
];
const steps = 13;

var lerpTemp = 0;
var lerpColors = [];
for (var i = 0; i < steps; i++)
  lerpColors.push([0, 0, 0]);

const lerpRatio = 0.1;
const defaultColor = [0, 255, 0];
var {start, step, color, blink} = ranges[0];

setInterval(() => {
  var res = ranges
    .filter(e => !e.min || lerpTemp >= e.min)
    .reverse()[0];

  start = res.start;
  step = res.step;
  color = res.color || defaultColor;
  blink = res.blink || false;

  lerpTemp += (tempFetch.temp - lerpTemp) * lerpRatio;
  lerpTemp = Math.min(lerpTemp, start + step * steps);

  for (let lerpColor of lerpColors)
    for (let n = 0; n < lerpColor.length; n++)
      lerpColor[n] += (color[n] - lerpColor[n]) * lerpRatio;
}, 100);

// Temperature meter (Esc through F12)
colorStack.addColorCallback((x, y) => {
  if (y == 0 && x < steps) {
    let i = x;
    let threshold = i*step + start;
    let previousStep = (i-1)*step;
    let ratio = ((lerpTemp - start) - previousStep) / step;
    ratio = Math.max(Math.min(ratio, 1), 0);

    let safeColor = threshold < 80
    ? lerpColors[i] || [0,255,0]
    : [255, 0, 0];

    let position = (i / steps)*0.8+0.2;
    let alpha = Math.min(Math.max(ratio*position, 0), 1);
    if (ratio < 0) alpha = 0;
    if (ratio > 1) alpha = 0;

    if (blink && Date.now()%1000 > 500)
      alpha = 1;

    return [...safeColor, alpha];
  }
})
