const logiled = require('logiled');

/**
 * Sets the color of the whole keyboard
 * @param {Number} r 0-255
 * @param {Number} g 0-255
 * @param {Number} b 0-255
 */
function all(r, g, b) {
  logiled.setLighting({
    redPercentage: r/2.55,
    greenPercentage: g/2.55,
    bluePercentage: b/2.55
  });
}

/**
 * Sets an individual key's color
 * @param {String} name
 * @param {Number} r 0-255
 * @param {Number} g 0-255
 * @param {Number} b 0-255
 */
function key(name, r, g, b, a) {
  if (!logiled.KeyName[name])
    throw new Error("Unknown key: " + name);
  if (isNaN(+a)) // test if number
    a = 1;
  logiled.setLightingForKeyWithKeyName({
    keyName: logiled.KeyName[name],
    redPercentage: Math.min(r/2.55, 255) * a,
    greenPercentage: Math.min(g/2.55, 255) * a,
    bluePercentage: Math.min(b/2.55, 255) * a
  });
}

/**
 * Blits an image from a colorStack's color stack using
 * logiled.setLightingFromBitmap()
 * @param {ColorStack} colorStack
 * @param {Boolean} drawGKeys draws macro keys and other g910 lights
 *   Isn't included in the normal bounds and calls getColor with
 *   out of bounds including negative arguments.
 */
function blit(colorStack, drawGKeys) {
  let bitmap = Buffer.alloc(logiled.LED_BITMAP_SIZE);
  for (let x = 0; x < logiled.LED_BITMAP_WIDTH; x++) {
    for (let y = 0; y < logiled.LED_BITMAP_HEIGHT; y++) {
      let i = (x + y * logiled.LED_BITMAP_WIDTH) * 4;
      let col = colorStack.getColor(x, y);
      // Logitech uses [B,G,R,A] for some godforsaken reason
      bitmap[i+0] = col[2];
      bitmap[i+1] = col[1];
      bitmap[i+2] = col[0];
      bitmap[i+3] = col[3]*255;
    }
  }
  if (drawGKeys) {
    key("G_1", ...colorStack.getColor(-1,  1));
    key("G_2", ...colorStack.getColor(-1,  2));
    key("G_3", ...colorStack.getColor(-1,  3));
    key("G_4", ...colorStack.getColor(-1,  4));
    key("G_5", ...colorStack.getColor(-1,  5));
    key("G_6", ...colorStack.getColor( 1, -1));
    key("G_7", ...colorStack.getColor( 2, -1));
    key("G_8", ...colorStack.getColor( 3, -1));
    key("G_9", ...colorStack.getColor( 4, -1));
    key("G_LOGO", ...colorStack.getColor(0, -1));
    key("G_BADGE", ...colorStack.getColor(2, 6));
  }
  logiled.setLightingFromBitmap({ bitmap: bitmap });
}

module.exports = { all, key, blit }
