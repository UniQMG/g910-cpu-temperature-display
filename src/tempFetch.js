const fetch = require('node-fetch');

var temp = 0;
var mode = 'cpu';
var historyLength = 50;

module.exports = {
  get temp() { return temp; },
  set mode(t) { mode = t; },
  set historyLength(h) {
    historyLength = h;
  }
};

const history = [];
const modes = {
  cpu: data => data
    .filter(e => /DTS/.test(e.SensorClass))
    .filter(e => /^Core (#\d)$|CPU Package$/i.test(e.SensorName))
    .filter(e => e.SensorUnit.endsWith('C'))
    .map(e => +e.SensorValue)
    .reduce((a,b) => Math.max(a,b)),
  gpu: data => data
      .filter(e => e.SensorName == 'GPU Temperature')
      .map(e => +e.SensorValue)
      .reduce((a,b) => Math.max(a,b))
};

function update() {
  fetch('http://localhost:55555').then(b => b.json()).then(data => {
    let newTemp = modes[mode](data);

    history.push(newTemp);
    if (history.length > historyLength)
      history.splice(0, history.length - historyLength)

    let avgTemp = history.reduce((a,b) => a+b) / history.length;
    temp = avgTemp;
  }).catch(ex => {
    console.error("Can't communicate with remote sensor monitor", ex);
    console.error("Is it running and accessible on port 55555?");
  });
}
setInterval(update, 100);
