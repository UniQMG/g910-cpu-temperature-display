const RED = 0;
const GREEN = 1;
const BLUE = 2;
const ALPHA = 3;

class ColorStack {
  /**
   * @param {Number[4]} bgColor
   */
  constructor(bgColor) {
    this.colorStack = [
      (x, y) => bgColor || [0,0,0,1]
    ];
  }

  /**
   * Adds a color callback, which functions somewhat like
   * a fragment shader.
   * The callback is passed an x and a y coordinate, and
   * returns the color at that coordinate as [r,g,b,a]
   * @param {Function<Number x, Number y>: Number[4]} callback
   */
  addColorCallback(callback) {
    this.colorStack.push(callback);
  }

  /**
   * @param {Function} callback
   */
  removeColorCallback(callback) {
    this.colorStack = this.colorStack.filter(c => c != callback);
  }

  /**
   * Blends two colors together. You probably have the parameters
   * backwards.
   * @param {Number[4]} c1
   * @param {Number[4]} c2
   * @return Number[4] color
   */
  static blend(c1, c2) {
    if (!c1) return c2 || [0,0,0,0];
    if (!c2) return c1 || [0,0,0,0];
    let [r1, g1, b1, a1] = c1;
    let [r2, g2, b2, a2] = c2;
    if (a1 == undefined) a1 = 1;
    if (a2 == undefined) a2 = 1;
    let na = a1 + a2 * (1 - a1); // New alpha
    return [
      (r1 * a1 + r2 * a2 * (1 - a1)) / na,
      (g1 * a1 + g2 * a2 * (1 - a1)) / na,
      (b1 * a1 + b2 * a2 * (1 - a1)) / na,
      na
    ];
  }

  /**
   * Calculates a color at a given coordinate
   * @param {Number} x
   * @param {Number} y
   * @return Number[4]
   */
  getColor(x,y) {
    return this.colorStack
      .map(callback => callback(x, y))
      .filter(a => a && a.length)
      .reverse()
      .reduce((c1, c2) => {
        return ColorStack.blend(c1, c2);
      });
  }
}


module.exports = ColorStack;
