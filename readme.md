Logitech LGS full RGB keyboard temperature display
Because normal animations are boring.

Doesn't do much else besides the temperature keys, but
should be pretty easy to modify if you want to make
your own effects.

**Consider this project abandoned, as it's a one-off
experiment and most likely won't have any form of
support or maintenance in the future.**

For example, creating a red gradient (`tempkey.js`):

    colorStack.addColorCallback((x, y) => {
      let xPosRatio = x / logiled.LED_BITMAP_WIDTH;

      let r = xPosRatio * 255;
      let g = 0;
      let b = 0;
      let a = 1.0;

      return [r, g, b, a];
    });

`addColorCallback` adds a layer to the draw call, and
layers are composited together using simple blending.

Windows-only, since LGS only runs on windows anyway.
(Thanks logitech)

Usage:
* Clone/Download repo
* Install [node](https://nodejs.org/en/) if you haven't already
* npm install
* node index.js

logiled has an issue with getting a correct prebuilt image, so you may need to build it yourself. See
https://www.npmjs.com/package/logiled for instructions.

Make sure you have the remote sensor and HWiNFO64 running in the background. Setting the update rate in settings to
100ms is recommended. By default it polls every 2 seconds.
* https://www.hwinfo.com/forum/Thread-Introducing-Remote-Sensor-Monitor-A-RESTful-Web-Server
* https://www.hwinfo.com/

How to change:
Send an http GET to one of the following:
* http://\<url\>:55556/switch/cpu
* http://\<url\>:55556/switch/gpu

If you have macro keys and chrome, you can do this without
opening a cmd window by using a 'Shortcut' macro:

    chrome --headless "http://localhost:55556/switch/cpu"

Default temperature ranges:
*  1C - 27C: Dark blue, 2C per step.
* 27C - 40C: Cyan/light blue, 1C per step.
* 40C - 90C: Green, then red above 80C. 4C per step.
* 90C+     : Blinking red, 1C per step.

0-9 will be lit up with exact temperature in 10C steps.
Above 110C all digits will light up, though most processors
will automatically shut down by this point.
